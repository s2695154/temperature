package nl.utwente.di.bookQuote;

public class Temperature {
    private double celsius;
    public Temperature(){

    }
    public double getFahrenheit(String celsius) {
        this.celsius = Double.valueOf(celsius);
        return this.celsius * 9 / 5 + 32;
    }
}
